

 Matrix histogram
 ----------------


 data   matrix        mf795 : DSTSK2                                   Distance Skim Car                                                               

 weight matrix        mf04  : temp4                                    Temporary Matrix 4                                                              

  all  origins   all  destinations

 constraint matrix    mf10  : temp10                                   Temporary Matrix 10                                                             
          interval  lower:         1
                    upper:         1  include 

 out of range       above:        1
                    below:        0

 statistics         mean : 38.41852
                    std  : 21.15928

    interval        ------density-------      -----cumulative-----
 (from  ,    to)    absolute  normalized      absolute  normalized

       0      1          209      .0036            209      .0036
       1      2          416      .0071            625      .0107
       2      3          916      .0157           1541      .0264
       3      4          462      .0079           2003      .0344
       4      5          205      .0035           2208      .0379
       5      6          575      .0099           2783      .0478
       6      7          674      .0116           3457      .0593
       7      8          729      .0125           4186      .0718
       8      9          474      .0081           4660      .0800
       9     10          240      .0041           4900      .0841
      10     11          487      .0084           5387      .0924
      11     12          985      .0169           6372      .1093
      12     13          435      .0075           6807      .1168
      13     14          501      .0086           7308      .1254
      14     15          611      .0105           7919      .1359
      15     16          857      .0147           8776      .1506
      16     17         1510      .0259          10286      .1765
      17     18         1072      .0184          11358      .1949
      18     19          533      .0091          11891      .2041
      19     20         1113      .0191          13004      .2232
      20     21          222      .0038          13226      .2270
      21     22          280      .0048          13506      .2318
      22     23          311      .0053          13817      .2371
      23     24          323      .0055          14140      .2427
      24     25          567      .0097          14707      .2524
      25     26          224      .0038          14931      .2562
      26     27         1194      .0205          16125      .2767
      27     28         1299      .0223          17424      .2990
      28     29          219      .0038          17643      .3028
      29     30         1265      .0217          18908      .3245
      30     31         1274      .0219          20182      .3463
      31     32         1966      .0337          22148      .3801
      32     33          475      .0082          22623      .3882
      33     34         1469      .0252          24092      .4134
      34     35         1997      .0343          26089      .4477
      35     36         1513      .0260          27602      .4737
      36     37          443      .0076          28045      .4813
      37     38         1511      .0259          29556      .5072
      38     39          977      .0168          30533      .5240
      39     40         1289      .0221          31822      .5461
      40     41          205      .0035          32027      .5496
      41     42         1688      .0290          33715      .5786
      42     43         1827      .0314          35542      .6099
      43     44         1275      .0219          36817      .6318
      44     45          852      .0146          37669      .6464
      45     46          807      .0138          38476      .6603
      46     47          725      .0124          39201      .6727
      47     48         2330      .0400          41531      .7127
      48     49          688      .0118          42219      .7245
      49     50          877      .0151          43096      .7396
      50     51          309      .0053          43405      .7449
      51     52         1098      .0188          44503      .7637
      52     53          801      .0137          45304      .7775
      53     54          190      .0033          45494      .7807
      54     55         2251      .0386          47745      .8193
      55     56         1028      .0176          48773      .8370
      56     57          404      .0069          49177      .8439
      57     58          532      .0091          49709      .8531
      58     59          345      .0059          50054      .8590
      59     60          845      .0145          50899      .8735
      60     61          525      .0090          51424      .8825
      61     62          372      .0064          51796      .8889
      62     63          330      .0057          52126      .8945
      63     64          201      .0034          52327      .8980
      64     65          512      .0088          52839      .9068
      65     66          616      .0106          53455      .9173
      66     67          234      .0040          53689      .9214
      67     68          379      .0065          54068      .9279
      68     69          354      .0061          54422      .9339
      69     70          297      .0051          54719      .9390
      70     71          183      .0031          54902      .9422
      71     72          234      .0040          55136      .9462
      72     73          289      .0050          55425      .9511
      73     74          246      .0042          55671      .9554
      74     75          367      .0063          56038      .9617
      75     76          235      .0040          56273      .9657
      76     77          103      .0018          56376      .9675
      77     78          196      .0034          56572      .9708
      78     79          250      .0043          56822      .9751
      79     80          210      .0036          57032      .9787
      80     81           53      .0009          57085      .9796
      81     82          200      .0034          57285      .9831
      82     83           70      .0012          57355      .9843
      83     84           32      .0005          57387      .9848
      84     85           57      .0010          57444      .9858
      85     86           47      .0008          57491      .9866
      86     87          107      .0018          57598      .9884
      87     88           37      .0006          57635      .9891
      88     89           53      .0009          57688      .9900
      89     90           16      .0003          57704      .9903
      90     91           53      .0009          57757      .9912
      91     92           27      .0005          57784      .9916
      92     93           40      .0007          57824      .9923
      93     94           19      .0003          57843      .9926
      94     95           13      .0002          57856      .9929
      95     96           25      .0004          57881      .9933
      96     97            3      .0001          57884      .9933
      97     98           22      .0004          57906      .9937
      98     99           19      .0003          57925      .9940
      99    100           15      .0003          57940      .9943
     100    101           16      .0003          57956      .9946
     101    102            5      .0001          57961      .9947
     102    103           11      .0002          57972      .9949
     103    104            4      .0001          57976      .9949
     104    105           16      .0003          57992      .9952
     105    106            8      .0001          58000      .9953
     106    107            7      .0001          58007      .9955
     107    108            6      .0001          58013      .9956
     108    109            3      .0001          58016      .9956
     109    110            5      .0001          58021      .9957
     110    111            8      .0001          58029      .9958
     111    112            9      .0002          58038      .9960
     112    113            1      .0000          58039      .9960
     113    114            6      .0001          58045      .9961
     114    115            4      .0001          58049      .9962
     115    116            6      .0001          58055      .9963
     116    117            1      .0000          58056      .9963
     117    118            5      .0001          58061      .9964
     118    119            5      .0001          58066      .9965
     119    120            9      .0002          58075      .9966
     120    121           12      .0002          58087      .9968
     121    122           10      .0002          58097      .9970
     122    123            9      .0002          58106      .9972
     123    124           10      .0002          58116      .9973
     124    125            1      .0000          58117      .9973
     125    126            3      .0001          58120      .9974
     126    127            7      .0001          58127      .9975
     127    128            1      .0000          58128      .9975
     128    129            4      .0001          58132      .9976
     129    130            2      .0000          58134      .9976
     130    131            2      .0000          58136      .9977
     131    132            2      .0000          58138      .9977
     132    133            1      .0000          58139      .9977
     133    134            5      .0001          58144      .9978
     134    135            6      .0001          58150      .9979
     135    136            3      .0001          58153      .9980
     136    137            3      .0001          58156      .9980
     137    138            9      .0002          58165      .9982
     138    139            8      .0001          58173      .9983
     139    140            1      .0000          58174      .9983
     140    141            2      .0000          58176      .9984
     141    142            0      .0000          58176      .9984
     142    143            5      .0001          58181      .9984
     143    144            5      .0001          58186      .9985
     144    145            2      .0000          58188      .9986
     145    146            2      .0000          58190      .9986
     146    147            6      .0001          58196      .9987
     147    148            0      .0000          58196      .9987
     148    149            1      .0000          58197      .9987
     149    150            1      .0000          58198      .9987
     150    151            1      .0000          58199      .9987
     151    152            2      .0000          58201      .9988
     152    153            1      .0000          58202      .9988
     153    154            0      .0000          58202      .9988
     154    155            1      .0000          58203      .9988
     155    156            2      .0000          58205      .9989
     156    157            3      .0001          58208      .9989
     157    158            5      .0001          58213      .9990
     158    159            3      .0001          58216      .9990
     159    160            2      .0000          58218      .9991
     160    161            0      .0000          58218      .9991
     161    162            1      .0000          58219      .9991
     162    163            2      .0000          58221      .9991
     163    164            0      .0000          58221      .9991
     164    165            2      .0000          58223      .9992
     165    166            0      .0000          58223      .9992
     166    167            0      .0000          58223      .9992
     167    168            1      .0000          58224      .9992
     168    169            1      .0000          58225      .9992
     169    170            1      .0000          58226      .9992
     170    171            1      .0000          58227      .9992
     171    172            0      .0000          58227      .9992
     172    173            1      .0000          58228      .9992
     173    174            2      .0000          58230      .9993
     174    175            1      .0000          58231      .9993
     175    176            0      .0000          58231      .9993
     176    177            2      .0000          58233      .9993
     177    178            0      .0000          58233      .9993
     178    179            3      .0001          58236      .9994
     179    180            0      .0000          58236      .9994
     180    181            1      .0000          58237      .9994
     181    182            0      .0000          58237      .9994
     182    183            1      .0000          58238      .9994
     183    184            1      .0000          58239      .9994
     184    185            1      .0000          58240      .9995
     185    186            0      .0000          58240      .9995
     186    187            1      .0000          58241      .9995
     187    188            0      .0000          58241      .9995
     188    189            0      .0000          58241      .9995
     189    190            0      .0000          58241      .9995
     190    191            1      .0000          58242      .9995
     191    192            0      .0000          58242      .9995
     192    193            0      .0000          58242      .9995
     193    194            0      .0000          58242      .9995
     194    195            0      .0000          58242      .9995
     195    196            0      .0000          58242      .9995
     196    197            0      .0000          58242      .9995
     197    198            0      .0000          58242      .9995
     198    199            0      .0000          58242      .9995
     199    200            1      .0000          58243      .9995
     200    201            0      .0000          58243      .9995
     201    202            0      .0000          58243      .9995
     202    203            0      .0000          58243      .9995
     203    204            0      .0000          58243      .9995
     204    205            1      .0000          58244      .9995
     205    206            0      .0000          58244      .9995
     206    207            0      .0000          58244      .9995
     207    208            0      .0000          58244      .9995
     208    209            0      .0000          58244      .9995
     209    210            0      .0000          58244      .9995
     210    211            0      .0000          58244      .9995
     211    212            0      .0000          58244      .9995
     212    213            0      .0000          58244      .9995
     213    214            0      .0000          58244      .9995
     214    215            0      .0000          58244      .9995
     215    216            0      .0000          58244      .9995
     216    217            0      .0000          58244      .9995
     217    218            0      .0000          58244      .9995
     218    219            0      .0000          58244      .9995
     219    220            0      .0000          58244      .9995
     220    221            0      .0000          58244      .9995
     221    222            0      .0000          58244      .9995
     222    223            0      .0000          58244      .9995
     223    224            0      .0000          58244      .9995
     224    225            0      .0000          58244      .9995
     225    226            0      .0000          58244      .9995
     226    227            0      .0000          58244      .9995
     227    228            0      .0000          58244      .9995
     228    229            0      .0000          58244      .9995
     229    230            2      .0000          58246      .9996
     230    231            0      .0000          58246      .9996
     231    232            0      .0000          58246      .9996
     232    233            0      .0000          58246      .9996
     233    234            0      .0000          58246      .9996
     234    235            0      .0000          58246      .9996
     235    236            0      .0000          58246      .9996
     236    237            0      .0000          58246      .9996
     237    238            0      .0000          58246      .9996
     238    239            0      .0000          58246      .9996
     239    240            0      .0000          58246      .9996
     240    241            0      .0000          58246      .9996
     241    242            0      .0000          58246      .9996
     242    243            0      .0000          58246      .9996
     243    244            0      .0000          58246      .9996
     244    245            0      .0000          58246      .9996
     245    246            1      .0000          58247      .9996
     246    247            0      .0000          58247      .9996
     247    248            0      .0000          58247      .9996
     248    249            0      .0000          58247      .9996
     249    250            1      .0000          58248      .9996
     250    251            0      .0000          58248      .9996
     251    252            0      .0000          58248      .9996
     252    253            1      .0000          58249      .9996
     253    254            0      .0000          58249      .9996
     254    255            0      .0000          58249      .9996
     255    256            0      .0000          58249      .9996
     256    257            1      .0000          58250      .9996
     257    258            0      .0000          58250      .9996
     258    259            0      .0000          58250      .9996
     259    260            0      .0000          58250      .9996
     260    261            0      .0000          58250      .9996
     261    262            0      .0000          58250      .9996
     262    263            1      .0000          58251      .9996
     263    264            0      .0000          58251      .9996
     264    265            0      .0000          58251      .9996
     265    266            1      .0000          58252      .9997
     266    267            1      .0000          58253      .9997
     267    268            0      .0000          58253      .9997
     268    269            0      .0000          58253      .9997
     269    270            0      .0000          58253      .9997
     270    271            0      .0000          58253      .9997
     271    272            0      .0000          58253      .9997
     272    273            0      .0000          58253      .9997
     273    274            0      .0000          58253      .9997
     274    275            0      .0000          58253      .9997
     275    276            0      .0000          58253      .9997
     276    277            1      .0000          58254      .9997
     277    278            4      .0001          58258      .9998
     278    279            0      .0000          58258      .9998
     279    280            1      .0000          58259      .9998
     280    281            0      .0000          58259      .9998
     281    282            0      .0000          58259      .9998
     282    283            0      .0000          58259      .9998
     283    284            1      .0000          58260      .9998
     284    285            0      .0000          58260      .9998
     285    286            0      .0000          58260      .9998
     286    287            0      .0000          58260      .9998
     287    288            0      .0000          58260      .9998
     288    289            0      .0000          58260      .9998
     289    290            0      .0000          58260      .9998
     290    291            1      .0000          58261      .9998
     291    292            0      .0000          58261      .9998
     292    293            0      .0000          58261      .9998
     293    294            0      .0000          58261      .9998
     294    295            0      .0000          58261      .9998
     295    296            0      .0000          58261      .9998
     296    297            0      .0000          58261      .9998
     297    298            0      .0000          58261      .9998
     298    299            0      .0000          58261      .9998
     299    300            0      .0000          58261      .9998
     300    301            0      .0000          58261      .9998
     301    302            0      .0000          58261      .9998
     302    303            0      .0000          58261      .9998
     303    304            0      .0000          58261      .9998
     304    305            0      .0000          58261      .9998
     305    306            0      .0000          58261      .9998
     306    307            0      .0000          58261      .9998
     307    308            0      .0000          58261      .9998
     308    309            0      .0000          58261      .9998
     309    310            0      .0000          58261      .9998
     310    311            0      .0000          58261      .9998
     311    312            0      .0000          58261      .9998
     312    313            0      .0000          58261      .9998
     313    314            0      .0000          58261      .9998
     314    315            0      .0000          58261      .9998
     315    316            2      .0000          58263      .9998
     316    317            0      .0000          58263      .9998
     317    318            0      .0000          58263      .9998
     318    319            0      .0000          58263      .9998
     319    320            1      .0000          58264      .9999
     320    321            0      .0000          58264      .9999
     321    322            0      .0000          58264      .9999
     322    323            1      .0000          58265      .9999
     323    324            0      .0000          58265      .9999
     324    325            1      .0000          58266      .9999
     325    326            0      .0000          58266      .9999
     326    327            1      .0000          58267      .9999
     327    328            0      .0000          58267      .9999
     328    329            0      .0000          58267      .9999
     329    330            0      .0000          58267      .9999
     330    331            0      .0000          58267      .9999
     331    332            1      .0000          58268      .9999
     332    333            0      .0000          58268      .9999
     333    334            0      .0000          58268      .9999
     334    335            0      .0000          58268      .9999
     335    336            0      .0000          58268      .9999
     336    337            0      .0000          58268      .9999
     337    338            0      .0000          58268      .9999
     338    339            1      .0000          58269      .9999
     339    340            0      .0000          58269      .9999
     340    341            0      .0000          58269      .9999
     341    342            0      .0000          58269      .9999
     342    343            0      .0000          58269      .9999
     343    344            2      .0000          58271     1.0000
     344    345            0      .0000          58271     1.0000
     345    346            0      .0000          58271     1.0000
     346    347            0      .0000          58271     1.0000
     347    348            0      .0000          58271     1.0000
     348    349            0      .0000          58271     1.0000
     349    350            0      .0000          58271     1.0000
     350    351            0      .0000          58271     1.0000
     351    352            0      .0000          58271     1.0000
     352    353            0      .0000          58271     1.0000
     353    354            0      .0000          58271     1.0000
     354    355            0      .0000          58271     1.0000
     355    356            0      .0000          58271     1.0000
     356    357            0      .0000          58271     1.0000
     357    358            0      .0000          58271     1.0000
     358    359            0      .0000          58271     1.0000
     359    360            0      .0000          58271     1.0000
     360    361            0      .0000          58271     1.0000
     361    362            0      .0000          58271     1.0000
     362    363            0      .0000          58271     1.0000
     363    364            0      .0000          58271     1.0000
     364    365            0      .0000          58271     1.0000
     365    366            0      .0000          58271     1.0000
     366    367            0      .0000          58271     1.0000
     367    368            0      .0000          58271     1.0000
     368    369            0      .0000          58271     1.0000
     369    370            0      .0000          58271     1.0000
     370    371            0      .0000          58271     1.0000
     371    372            0      .0000          58271     1.0000
     372    373            0      .0000          58271     1.0000
     373    374            0      .0000          58271     1.0000
     374    375            0      .0000          58271     1.0000
     375    376            0      .0000          58271     1.0000
     376    377            0      .0000          58271     1.0000
     377    378            0      .0000          58271     1.0000
     378    379            0      .0000          58271     1.0000
     379    380            0      .0000          58271     1.0000
     380    381            0      .0000          58271     1.0000
     381    382            0      .0000          58271     1.0000
     382    383            0      .0000          58271     1.0000
     383    384            0      .0000          58271     1.0000
     384    385            0      .0000          58271     1.0000
     385    386            0      .0000          58271     1.0000
     386    387            0      .0000          58271     1.0000
     387    388            0      .0000          58271     1.0000
     388    389            0      .0000          58271     1.0000
     389    390            0      .0000          58271     1.0000
     390    391            0      .0000          58271     1.0000
     391    392            0      .0000          58271     1.0000
     392    393            0      .0000          58271     1.0000
     393    394            0      .0000          58271     1.0000
     394    395            0      .0000          58271     1.0000
     395    396            0      .0000          58271     1.0000
     396    397            0      .0000          58271     1.0000
     397    398            0      .0000          58271     1.0000
     398    399            0      .0000          58271     1.0000
     399    400            0      .0000          58271     1.0000
     400    401            0      .0000          58271     1.0000
     401    402            0      .0000          58271     1.0000
     402    403            0      .0000          58271     1.0000
     403    404            0      .0000          58271     1.0000
     404    405            0      .0000          58271     1.0000
     405    406            0      .0000          58271     1.0000
     406    407            0      .0000          58271     1.0000
     407    408            0      .0000          58271     1.0000
     408    409            0      .0000          58271     1.0000
     409    410            0      .0000          58271     1.0000
     410    411            0      .0000          58271     1.0000
     411    412            0      .0000          58271     1.0000
     412    413            0      .0000          58271     1.0000
     413    414            0      .0000          58271     1.0000
     414    415            0      .0000          58271     1.0000
     415    416            0      .0000          58271     1.0000
     416    417            0      .0000          58271     1.0000
     417    418            0      .0000          58271     1.0000
     418    419            0      .0000          58271     1.0000
     419    420            0      .0000          58271     1.0000
     420    421            0      .0000          58271     1.0000
     421    422            0      .0000          58271     1.0000
     422    423            0      .0000          58271     1.0000
     423    424            0      .0000          58271     1.0000
     424    425            0      .0000          58271     1.0000
     425    426            0      .0000          58271     1.0000
     426    427            0      .0000          58271     1.0000
     427    428            0      .0000          58271     1.0000
     428    429            0      .0000          58271     1.0000
     429    430            0      .0000          58271     1.0000
     430    431            0      .0000          58271     1.0000
     431    432            0      .0000          58271     1.0000
     432    433            0      .0000          58271     1.0000
     433    434            0      .0000          58271     1.0000
     434    435            0      .0000          58271     1.0000
     435    436            0      .0000          58271     1.0000
     436    437            0      .0000          58271     1.0000
     437    438            0      .0000          58271     1.0000
     438    439            0      .0000          58271     1.0000
     439    440            0      .0000          58271     1.0000
     440    441            0      .0000          58271     1.0000
     441    442            0      .0000          58271     1.0000
     442    443            0      .0000          58271     1.0000
     443    444            0      .0000          58271     1.0000
     444    445            0      .0000          58271     1.0000
     445    446            0      .0000          58271     1.0000
     446    447            0      .0000          58271     1.0000
     447    448            0      .0000          58271     1.0000
     448    449            0      .0000          58271     1.0000
     449    450            0      .0000          58271     1.0000
     450    451            0      .0000          58271     1.0000
     451    452            0      .0000          58271     1.0000
     452    453            0      .0000          58271     1.0000
     453    454            0      .0000          58271     1.0000
     454    455            0      .0000          58271     1.0000
     455    456            0      .0000          58271     1.0000
     456    457            0      .0000          58271     1.0000
     457    458            0      .0000          58271     1.0000
     458    459            0      .0000          58271     1.0000
     459    460            0      .0000          58271     1.0000
     460    461            0      .0000          58271     1.0000
     461    462            0      .0000          58271     1.0000
     462    463            0      .0000          58271     1.0000
     463    464            0      .0000          58271     1.0000
     464    465            0      .0000          58271     1.0000
     465    466            0      .0000          58271     1.0000
     466    467            0      .0000          58271     1.0000
     467    468            0      .0000          58271     1.0000
     468    469            0      .0000          58271     1.0000
     469    470            0      .0000          58271     1.0000
     470    471            0      .0000          58271     1.0000
     471    472            0      .0000          58271     1.0000
     472    473            0      .0000          58271     1.0000
     473    474            0      .0000          58271     1.0000
     474    475            0      .0000          58271     1.0000
     475    476            0      .0000          58271     1.0000
     476    477            0      .0000          58271     1.0000
     477    478            0      .0000          58271     1.0000
     478    479            0      .0000          58271     1.0000
     479    480            0      .0000          58271     1.0000
     480    481            0      .0000          58271     1.0000
     481    482            0      .0000          58271     1.0000
     482    483            0      .0000          58271     1.0000
     483    484            0      .0000          58271     1.0000
     484    485            0      .0000          58271     1.0000
     485    486            0      .0000          58271     1.0000
     486    487            0      .0000          58271     1.0000
     487    488            0      .0000          58271     1.0000
     488    489            0      .0000          58271     1.0000
     489    490            0      .0000          58271     1.0000
     490    491            0      .0000          58271     1.0000
     491    492            0      .0000          58271     1.0000
     492    493            0      .0000          58271     1.0000
     493    494            0      .0000          58271     1.0000
     494    495            0      .0000          58271     1.0000
     495    496            0      .0000          58271     1.0000
     496    497            0      .0000          58271     1.0000
     497    498            0      .0000          58271     1.0000
     498    499            0      .0000          58271     1.0000
     499    500            0      .0000          58271     1.0000


 MATRIX CALCULATIONS
 *******************


 Expression:

  mf05   = 0

 Result matrix:         mf05  : temp5                                         Temporary Matrix 5                                                               (18-11-21 10:40:07)

 Constraint matrix:     none

 Submatrix:             all origins             all destinations

 Number of expression evaluations:          35721
 Minimum expression value:                .000000   (at      1      1)
 Maximum expression value:                .000000   (at      1      1)
 Average expression value:                .000000
 Sum of expression values:                .000000


 MATRIX CALCULATIONS
 *******************


 Expression:

  mf05   = mf519' + mf520

 Result matrix:         mf05  : temp5                                         Temporary Matrix 5                                                               (18-11-21 10:40:07)

 Data matrices:         mf519 : MND353                                        Mobile PM HBOEIB Rail                                                            (18-11-21 10:39:19)
                        mf520 : MND354                                        Mobile PM HBOEOB Rail                                                            (18-11-21 10:39:21)

 Constraint matrix:     none

 Submatrix:             all origins             all destinations

 Number of expression evaluations:          35721
 Minimum expression value:                .000000   (at      1      1)
 Maximum expression value:           14735.000000   (at    102    106)
 Average expression value:               7.090675
 Sum of expression values:          253286.000000
