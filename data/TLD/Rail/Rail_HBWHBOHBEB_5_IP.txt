

 Matrix histogram
 ----------------


 data   matrix        mf795 : DSTSK2                                   Distance Skim Car                                                               

 weight matrix        mf01  : temp1                                    Distance Skim Car km                                                            

  all  origins   all  destinations

 constraint matrix    mf10  : temp10                                   Temporary Matrix 10                                                             
          interval  lower:         1
                    upper:         1  include 

 out of range       above: 24.93763
                    below:        0

 statistics         mean : 39.43376
                    std  : 42.00386

    interval        ------density-------      -----cumulative-----
 (from  ,    to)    absolute  normalized      absolute  normalized

       0      1     135.0222      .0049       135.0222      .0049
       1      2     211.8704      .0076       346.8927      .0125
       2      3     870.8309      .0313       1217.723      .0438
       3      4     73.17781      .0026       1290.901      .0464
       4      5     90.57431      .0033       1381.476      .0497
       5      6     799.2238      .0287       2180.699      .0784
       6      7     483.1727      .0174       2663.872      .0958
       7      8     317.5278      .0114         2981.4      .1072
       8      9     26.71421      .0010       3008.114      .1082
       9     10      68.0252      .0024       3076.139      .1106
      10     11     1094.096      .0393       4170.235      .1500
      11     12     462.1558      .0166       4632.391      .1666
      12     13     384.2653      .0138       5016.656      .1804
      13     14      244.701      .0088       5261.357      .1892
      14     15     62.46021      .0022       5323.817      .1914
      15     16      366.023      .0132        5689.84      .2046
      16     17     564.5839      .0203       6254.424      .2249
      17     18     337.2014      .0121       6591.625      .2370
      18     19     193.2032      .0069       6784.829      .2440
      19     20     1684.283      .0606       8469.112      .3045
      20     21     92.33947      .0033       8561.452      .3078
      21     22     165.8323      .0060       8727.284      .3138
      22     23      218.956      .0079        8946.24      .3217
      23     24     150.1393      .0054       9096.379      .3271
      24     25      190.315      .0068       9286.695      .3339
      25     26     96.55634      .0035       9383.251      .3374
      26     27     235.0286      .0085        9618.28      .3458
      27     28     46.46271      .0017       9664.743      .3475
      28     29     59.32419      .0021       9724.067      .3497
      29     30      494.263      .0178       10218.33      .3674
      30     31     915.4346      .0329       11133.76      .4003
      31     32     1299.678      .0467       12433.44      .4471
      32     33     49.70056      .0018       12483.14      .4489
      33     34      1935.53      .0696       14418.67      .5185
      34     35     1271.306      .0457       15689.98      .5642
      35     36     138.3785      .0050       15828.36      .5691
      36     37     34.73787      .0012        15863.1      .5704
      37     38     199.9607      .0072       16063.06      .5776
      38     39     72.21905      .0026       16135.28      .5802
      39     40     724.4338      .0260       16859.71      .6062
      40     41      3.98663      .0001        16863.7      .6064
      41     42     1539.254      .0553       18402.95      .6617
      42     43     292.1039      .0105       18695.05      .6722
      43     44     68.03668      .0024       18763.09      .6747
      44     45     78.64046      .0028       18841.73      .6775
      45     46     17.81926      .0006       18859.55      .6781
      46     47     544.0966      .0196       19403.65      .6977
      47     48     1549.412      .0557       20953.06      .7534
      48     49     37.84397      .0014        20990.9      .7548
      49     50     65.59561      .0024        21056.5      .7571
      50     51     165.8636      .0060       21222.36      .7631
      51     52     792.2335      .0285       22014.59      .7916
      52     53     581.9043      .0209        22596.5      .8125
      53     54     368.2088      .0132       22964.71      .8258
      54     55     513.8651      .0185       23478.57      .8442
      55     56     144.7498      .0052       23623.32      .8494
      56     57     152.9486      .0055       23776.27      .8549
      57     58     74.56442      .0027       23850.84      .8576
      58     59     168.2584      .0061       24019.09      .8637
      59     60      1113.85      .0401       25132.94      .9037
      60     61     14.18412      .0005       25147.13      .9042
      61     62     31.18162      .0011       25178.31      .9053
      62     63      4.69031      .0002          25183      .9055
      63     64     23.62136      .0008       25206.62      .9064
      64     65     206.7316      .0074       25413.35      .9138
      65     66     27.00117      .0010       25440.35      .9148
      66     67     184.5304      .0066       25624.89      .9214
      67     68     17.03005      .0006       25641.91      .9220
      68     69     465.1741      .0167       26107.09      .9387
      69     70     13.97106      .0005       26121.06      .9392
      70     71     246.0654      .0088       26367.12      .9481
      71     72     98.60916      .0035       26465.73      .9516
      72     73      10.7258      .0004       26476.46      .9520
      73     74     154.8385      .0056        26631.3      .9576
      74     75     16.66571      .0006       26647.96      .9582
      75     76     12.86615      .0005       26660.83      .9587
      76     77     12.98875      .0005       26673.82      .9591
      77     78      4.19914      .0002       26678.02      .9593
      78     79      5.88344      .0002        26683.9      .9595
      79     80     93.01928      .0033       26776.92      .9628
      80     81     34.77661      .0013        26811.7      .9641
      81     82      9.69076      .0003       26821.39      .9644
      82     83      1.91023      .0001        26823.3      .9645
      83     84      2.88234      .0001       26826.18      .9646
      84     85     11.93783      .0004       26838.12      .9650
      85     86      9.59939      .0003       26847.72      .9654
      86     87     19.40965      .0007       26867.13      .9661
      87     88      12.2072      .0004       26879.33      .9665
      88     89     8.508379      .0003       26887.84      .9668
      89     90       6.2425      .0002       26894.08      .9670
      90     91     11.41578      .0004        26905.5      .9675
      91     92      15.0336      .0005       26920.53      .9680
      92     93     6.436211      .0002       26926.97      .9682
      93     94      4.73898      .0002       26931.71      .9684
      94     95      1.17137      .0000       26932.88      .9684
      95     96     10.69093      .0004       26943.57      .9688
      96     97      2.04599      .0001       26945.62      .9689
      97     98      8.35774      .0003       26953.98      .9692
      98     99     12.01644      .0004       26965.99      .9696
      99    100      3.76199      .0001       26969.75      .9698
     100    101      6.95809      .0003       26976.71      .9700
     101    102      1.71044      .0001       26978.42      .9701
     102    103     14.14073      .0005       26992.56      .9706
     103    104     8.938231      .0003        27001.5      .9709
     104    105     8.739891      .0003       27010.24      .9712
     105    106     31.19892      .0011       27041.44      .9723
     106    107      3.01606      .0001       27044.46      .9724
     107    108      2.88141      .0001       27047.34      .9726
     108    109      3.22099      .0001       27050.56      .9727
     109    110        .4773      .0000       27051.03      .9727
     110    111     8.239681      .0003       27059.27      .9730
     111    112     10.83584      .0004       27070.11      .9734
     112    113      2.10293      .0001       27072.21      .9734
     113    114     13.09139      .0005       27085.31      .9739
     114    115      2.65644      .0001       27087.96      .9740
     115    116      2.63932      .0001        27090.6      .9741
     116    117      1.34165      .0000       27091.94      .9742
     117    118      6.15563      .0002        27098.1      .9744
     118    119     16.38318      .0006       27114.48      .9750
     119    120      3.24824      .0001       27117.73      .9751
     120    121      3.47973      .0001       27121.21      .9752
     121    122      3.53705      .0001       27124.75      .9753
     122    123     11.89722      .0004       27136.64      .9758
     123    124      9.41735      .0003       27146.06      .9761
     124    125      4.21441      .0002       27150.28      .9763
     125    126     3.814089      .0001       27154.09      .9764
     126    127      4.37224      .0002       27158.46      .9765
     127    128      3.81545      .0001       27162.28      .9767
     128    129     11.25065      .0004       27173.53      .9771
     129    130      6.01932      .0002       27179.55      .9773
     130    131      2.77006      .0001       27182.32      .9774
     131    132      3.50851      .0001       27185.83      .9775
     132    133     8.222532      .0003       27194.05      .9778
     133    134      5.59313      .0002       27199.64      .9780
     134    135      1.79516      .0001       27201.44      .9781
     135    136      2.76403      .0001        27204.2      .9782
     136    137       .92251      .0000       27205.12      .9782
     137    138      5.70648      .0002       27210.83      .9784
     138    139     60.78622      .0022       27271.62      .9806
     139    140      1.17591      .0000       27272.79      .9807
     140    141      1.47212      .0001       27274.27      .9807
     141    142      1.34152      .0000       27275.61      .9808
     142    143      3.94615      .0001       27279.55      .9809
     143    144      3.53495      .0001       27283.09      .9810
     144    145      8.38518      .0003       27291.47      .9813
     145    146      2.07088      .0001       27293.54      .9814
     146    147      6.29458      .0002       27299.84      .9816
     147    148      3.25592      .0001       27303.09      .9817
     148    149      3.80539      .0001        27306.9      .9819
     149    150      4.53526      .0002       27311.43      .9820
     150    151       1.4495      .0001       27312.88      .9821
     151    152      3.49786      .0001       27316.38      .9822
     152    153      3.12518      .0001       27319.51      .9823
     153    154      3.14824      .0001       27322.66      .9825
     154    155      1.12583      .0000       27323.78      .9825
     155    156      3.13529      .0001       27326.91      .9826
     156    157     9.726531      .0003       27336.64      .9830
     157    158      3.33222      .0001       27339.97      .9831
     158    159      1.63432      .0001       27341.61      .9831
     159    160      4.20543      .0002       27345.81      .9833
     160    161      6.41671      .0002       27352.23      .9835
     161    162      1.23895      .0000       27353.47      .9836
     162    163      3.02885      .0001        27356.5      .9837
     163    164      3.34564      .0001       27359.84      .9838
     164    165     3.459331      .0001        27363.3      .9839
     165    166      2.85258      .0001       27366.16      .9840
     166    167       .25416      .0000       27366.41      .9840
     167    168      1.69986      .0001       27368.11      .9841
     168    169      2.71703      .0001       27370.83      .9842
     169    170       .57655      .0000        27371.4      .9842
     170    171      3.61014      .0001       27375.01      .9843
     171    172       .22908      .0000       27375.24      .9843
     172    173      2.00735      .0001       27377.25      .9844
     173    174      2.97264      .0001       27380.22      .9845
     174    175      1.55069      .0001       27381.77      .9846
     175    176        .3392      .0000       27382.11      .9846
     176    177      1.74481      .0001       27383.85      .9847
     177    178       .11876      .0000       27383.97      .9847
     178    179      2.08396      .0001       27386.06      .9847
     179    180       .66524      .0000       27386.72      .9848
     180    181       .72269      .0000       27387.45      .9848
     181    182        .0279      .0000       27387.47      .9848
     182    183      1.72857      .0001        27389.2      .9848
     183    184       .77289      .0000       27389.98      .9849
     184    185      1.30512      .0000       27391.28      .9849
     185    186       .51623      .0000        27391.8      .9849
     186    187       .46526      .0000       27392.26      .9850
     187    188       .40166      .0000       27392.66      .9850
     188    189       .02459      .0000       27392.69      .9850
     189    190       .07608      .0000       27392.76      .9850
     190    191      1.27079      .0000       27394.04      .9850
     191    192        .7819      .0000       27394.82      .9850
     192    193      1.34391      .0000       27396.16      .9851
     193    194       .46881      .0000       27396.63      .9851
     194    195       .58851      .0000       27397.22      .9851
     195    196      1.19722      .0000       27398.41      .9852
     196    197        .6722      .0000       27399.09      .9852
     197    198       .79793      .0000       27399.89      .9852
     198    199       .44423      .0000       27400.33      .9852
     199    200      1.63393      .0001       27401.96      .9853
     200    201       .41811      .0000       27402.38      .9853
     201    202       .31065      .0000       27402.69      .9853
     202    203       .03299      .0000       27402.73      .9853
     203    204       .96116      .0000       27403.69      .9854
     204    205       .29913      .0000       27403.99      .9854
     205    206      1.65337      .0001       27405.64      .9854
     206    207       .11981      .0000       27405.76      .9854
     207    208       .15859      .0000       27405.92      .9854
     208    209      2.18237      .0001        27408.1      .9855
     209    210        .2199      .0000       27408.32      .9855
     210    211       .19672      .0000       27408.52      .9855
     211    212       .09176      .0000       27408.61      .9855
     212    213       .18508      .0000       27408.79      .9856
     213    214      1.40534      .0001        27410.2      .9856
     214    215       .25978      .0000       27410.46      .9856
     215    216       .08975      .0000       27410.55      .9856
     216    217       .14894      .0000        27410.7      .9856
     217    218       .73622      .0000       27411.43      .9856
     218    219       .43818      .0000       27411.87      .9857
     219    220       .97144      .0000       27412.84      .9857
     220    221       .04748      .0000       27412.89      .9857
     221    222        .0042      .0000       27412.89      .9857
     222    223       .14261      .0000       27413.04      .9857
     223    224       .22081      .0000       27413.26      .9857
     224    225      2.75055      .0001       27416.01      .9858
     225    226       .05036      .0000       27416.06      .9858
     226    227       .26717      .0000       27416.33      .9858
     227    228       .15347      .0000       27416.48      .9858
     228    229       .35574      .0000       27416.83      .9858
     229    230      1.94274      .0001       27418.78      .9859
     230    231       .03034      .0000       27418.81      .9859
     231    232        .2522      .0000       27419.06      .9859
     232    233            0      .0000       27419.06      .9859
     233    234      2.31406      .0001       27421.38      .9860
     234    235      2.70378      .0001       27424.08      .9861
     235    236       .02651      .0000       27424.11      .9861
     236    237       .42904      .0000       27424.54      .9861
     237    238       .64232      .0000       27425.18      .9861
     238    239      1.28264      .0000       27426.46      .9862
     239    240      1.37387      .0000       27427.83      .9862
     240    241       .05833      .0000       27427.89      .9862
     241    242       .41418      .0000       27428.31      .9863
     242    243      1.23536      .0000       27429.54      .9863
     243    244      1.19495      .0000       27430.74      .9863
     244    245       .02566      .0000       27430.76      .9863
     245    246       .25608      .0000       27431.02      .9863
     246    247       .44245      .0000       27431.46      .9864
     247    248       2.5253      .0001       27433.99      .9865
     248    249      1.84221      .0001       27435.83      .9865
     249    250      1.41217      .0001       27437.24      .9866
     250    251       .32208      .0000       27437.57      .9866
     251    252       .51537      .0000       27438.08      .9866
     252    253      2.69608      .0001       27440.78      .9867
     253    254      2.40225      .0001       27443.18      .9868
     254    255      2.03869      .0001       27445.22      .9869
     255    256       .14496      .0000       27445.36      .9869
     256    257       .03954      .0000        27445.4      .9869
     257    258       .26807      .0000       27445.67      .9869
     258    259      5.32569      .0002          27451      .9871
     259    260      1.54585      .0001       27452.54      .9871
     260    261       .94576      .0000       27453.49      .9872
     261    262       .87421      .0000       27454.36      .9872
     262    263      9.85646      .0004       27464.22      .9875
     263    264     54.39096      .0020       27518.61      .9895
     264    265      1.03972      .0000       27519.65      .9895
     265    266      3.15672      .0001        27522.8      .9896
     266    267       .35452      .0000       27523.16      .9897
     267    268      1.75124      .0001       27524.91      .9897
     268    269      2.45941      .0001       27527.37      .9898
     269    270      2.66553      .0001       27530.04      .9899
     270    271      1.33389      .0000       27531.37      .9900
     271    272       .31819      .0000       27531.69      .9900
     272    273            0      .0000       27531.69      .9900
     273    274      9.30308      .0003       27540.99      .9903
     274    275      3.07972      .0001       27544.07      .9904
     275    276       .99978      .0000       27545.07      .9905
     276    277      4.11382      .0001       27549.18      .9906
     277    278      5.42912      .0002       27554.61      .9908
     278    279       .25694      .0000       27554.87      .9908
     279    280      3.58247      .0001       27558.45      .9909
     280    281      4.38376      .0002       27562.84      .9911
     281    282       .39438      .0000       27563.23      .9911
     282    283       3.9375      .0001       27567.17      .9912
     283    284       1.3548      .0000       27568.52      .9913
     284    285            0      .0000       27568.52      .9913
     285    286     8.383172      .0003       27576.91      .9916
     286    287       .63011      .0000       27577.54      .9916
     287    288      1.77342      .0001       27579.31      .9917
     288    289       .00357      .0000       27579.32      .9917
     289    290        .3568      .0000       27579.67      .9917
     290    291      1.65174      .0001       27581.33      .9918
     291    292       .46628      .0000       27581.79      .9918
     292    293       .62697      .0000       27582.42      .9918
     293    294       .06778      .0000       27582.49      .9918
     294    295       .04009      .0000       27582.53      .9918
     295    296      6.65668      .0002       27589.18      .9920
     296    297       .29195      .0000       27589.48      .9920
     297    298      7.10951      .0003       27596.58      .9923
     298    299      9.43947      .0003       27606.02      .9926
     299    300      1.78921      .0001       27607.81      .9927
     300    301        .0006      .0000       27607.81      .9927
     301    302            0      .0000       27607.81      .9927
     302    303       .07833      .0000       27607.89      .9927
     303    304        .2647      .0000       27608.16      .9927
     304    305       .33867      .0000        27608.5      .9927
     305    306      1.31162      .0000       27609.81      .9928
     306    307       .31479      .0000       27610.12      .9928
     307    308       .45449      .0000       27610.58      .9928
     308    309        .0447      .0000       27610.62      .9928
     309    310       .10011      .0000       27610.72      .9928
     310    311      1.46764      .0001       27612.19      .9929
     311    312      2.13639      .0001       27614.33      .9929
     312    313            0      .0000       27614.33      .9929
     313    314       .69468      .0000       27615.02      .9930
     314    315       1.1698      .0000       27616.19      .9930
     315    316     10.82999      .0004       27627.02      .9934
     316    317            0      .0000       27627.02      .9934
     317    318       .38865      .0000       27627.41      .9934
     318    319      3.14398      .0001       27630.55      .9935
     319    320      2.63516      .0001       27633.19      .9936
     320    321      7.57543      .0003       27640.76      .9939
     321    322            0      .0000       27640.76      .9939
     322    323            0      .0000       27640.76      .9939
     323    324       .07772      .0000       27640.84      .9939
     324    325      5.50916      .0002       27646.35      .9941
     325    326      8.37596      .0003       27654.73      .9944
     326    327     10.61725      .0004       27665.34      .9948
     327    328       .85026      .0000       27666.19      .9948
     328    329            0      .0000       27666.19      .9948
     329    330            0      .0000       27666.19      .9948
     330    331      1.86146      .0001       27668.06      .9949
     331    332     61.98337      .0022       27730.04      .9971
     332    333            0      .0000       27730.04      .9971
     333    334      6.61688      .0002       27736.66      .9973
     334    335            0      .0000       27736.66      .9973
     335    336            0      .0000       27736.66      .9973
     336    337            0      .0000       27736.66      .9973
     337    338       .14594      .0000        27736.8      .9973
     338    339            0      .0000        27736.8      .9973
     339    340            0      .0000        27736.8      .9973
     340    341            0      .0000        27736.8      .9973
     341    342            0      .0000        27736.8      .9973
     342    343            0      .0000        27736.8      .9973
     343    344     10.53069      .0004       27747.33      .9977
     344    345            0      .0000       27747.33      .9977
     345    346            0      .0000       27747.33      .9977
     346    347            0      .0000       27747.33      .9977
     347    348            0      .0000       27747.33      .9977
     348    349            0      .0000       27747.33      .9977
     349    350            0      .0000       27747.33      .9977
     350    351            0      .0000       27747.33      .9977
     351    352            0      .0000       27747.33      .9977
     352    353            0      .0000       27747.33      .9977
     353    354            0      .0000       27747.33      .9977
     354    355      1.44986      .0001       27748.78      .9978
     355    356            0      .0000       27748.78      .9978
     356    357            0      .0000       27748.78      .9978
     357    358      3.54341      .0001       27752.33      .9979
     358    359       .30276      .0000       27752.63      .9979
     359    360       .32362      .0000       27752.95      .9979
     360    361       .03621      .0000       27752.99      .9979
     361    362            0      .0000       27752.99      .9979
     362    363            0      .0000       27752.99      .9979
     363    364            0      .0000       27752.99      .9979
     364    365            0      .0000       27752.99      .9979
     365    366            0      .0000       27752.99      .9979
     366    367            0      .0000       27752.99      .9979
     367    368       .77621      .0000       27753.76      .9980
     368    369            0      .0000       27753.76      .9980
     369    370       .73716      .0000        27754.5      .9980
     370    371            0      .0000        27754.5      .9980
     371    372            0      .0000        27754.5      .9980
     372    373      1.12366      .0000       27755.62      .9980
     373    374        .0048      .0000       27755.63      .9980
     374    375            0      .0000       27755.63      .9980
     375    376            0      .0000       27755.63      .9980
     376    377            0      .0000       27755.63      .9980
     377    378            0      .0000       27755.63      .9980
     378    379      1.71664      .0001       27757.34      .9981
     379    380            0      .0000       27757.34      .9981
     380    381       .21997      .0000       27757.57      .9981
     381    382       .19276      .0000       27757.76      .9981
     382    383       .11633      .0000       27757.88      .9981
     383    384     12.96199      .0005       27770.84      .9986
     384    385            0      .0000       27770.84      .9986
     385    386            0      .0000       27770.84      .9986
     386    387       .15992      .0000          27771      .9986
     387    388            0      .0000          27771      .9986
     388    389       .41817      .0000       27771.42      .9986
     389    390       .37449      .0000       27771.79      .9986
     390    391       .43505      .0000       27772.23      .9986
     391    392            0      .0000       27772.23      .9986
     392    393       .07395      .0000        27772.3      .9986
     393    394       .51347      .0000       27772.82      .9986
     394    395       .28043      .0000        27773.1      .9986
     395    396            0      .0000        27773.1      .9986
     396    397       .42101      .0000       27773.52      .9987
     397    398       1.1321      .0000       27774.65      .9987
     398    399      1.15571      .0000       27775.81      .9987
     399    400            0      .0000       27775.81      .9987
     400    401            0      .0000       27775.81      .9987
     401    402            0      .0000       27775.81      .9987
     402    403      2.54534      .0001       27778.35      .9988
     403    404      1.04806      .0000        27779.4      .9989
     404    405            0      .0000        27779.4      .9989
     405    406            0      .0000        27779.4      .9989
     406    407            0      .0000        27779.4      .9989
     407    408      1.48134      .0001       27780.88      .9989
     408    409            0      .0000       27780.88      .9989
     409    410      1.17701      .0000       27782.06      .9990
     410    411       .27703      .0000       27782.34      .9990
     411    412            0      .0000       27782.34      .9990
     412    413            0      .0000       27782.34      .9990
     413    414            0      .0000       27782.34      .9990
     414    415      3.38282      .0001       27785.72      .9991
     415    416            0      .0000       27785.72      .9991
     416    417            0      .0000       27785.72      .9991
     417    418            0      .0000       27785.72      .9991
     418    419            0      .0000       27785.72      .9991
     419    420            0      .0000       27785.72      .9991
     420    421            0      .0000       27785.72      .9991
     421    422            0      .0000       27785.72      .9991
     422    423            0      .0000       27785.72      .9991
     423    424            0      .0000       27785.72      .9991
     424    425            0      .0000       27785.72      .9991
     425    426            0      .0000       27785.72      .9991
     426    427            0      .0000       27785.72      .9991
     427    428            0      .0000       27785.72      .9991
     428    429            0      .0000       27785.72      .9991
     429    430            0      .0000       27785.72      .9991
     430    431            0      .0000       27785.72      .9991
     431    432            0      .0000       27785.72      .9991
     432    433            0      .0000       27785.72      .9991
     433    434            0      .0000       27785.72      .9991
     434    435            0      .0000       27785.72      .9991
     435    436            0      .0000       27785.72      .9991
     436    437            0      .0000       27785.72      .9991
     437    438            0      .0000       27785.72      .9991
     438    439            0      .0000       27785.72      .9991
     439    440            0      .0000       27785.72      .9991
     440    441            0      .0000       27785.72      .9991
     441    442            0      .0000       27785.72      .9991
     442    443            0      .0000       27785.72      .9991
     443    444            0      .0000       27785.72      .9991
     444    445            0      .0000       27785.72      .9991
     445    446            0      .0000       27785.72      .9991
     446    447            0      .0000       27785.72      .9991
     447    448            0      .0000       27785.72      .9991
     448    449            0      .0000       27785.72      .9991
     449    450            0      .0000       27785.72      .9991
     450    451            0      .0000       27785.72      .9991
     451    452            0      .0000       27785.72      .9991
     452    453            0      .0000       27785.72      .9991
     453    454            0      .0000       27785.72      .9991
     454    455            0      .0000       27785.72      .9991
     455    456            0      .0000       27785.72      .9991
     456    457            0      .0000       27785.72      .9991
     457    458            0      .0000       27785.72      .9991
     458    459            0      .0000       27785.72      .9991
     459    460            0      .0000       27785.72      .9991
     460    461            0      .0000       27785.72      .9991
     461    462            0      .0000       27785.72      .9991
     462    463            0      .0000       27785.72      .9991
     463    464            0      .0000       27785.72      .9991
     464    465            0      .0000       27785.72      .9991
     465    466            0      .0000       27785.72      .9991
     466    467            0      .0000       27785.72      .9991
     467    468            0      .0000       27785.72      .9991
     468    469            0      .0000       27785.72      .9991
     469    470            0      .0000       27785.72      .9991
     470    471            0      .0000       27785.72      .9991
     471    472            0      .0000       27785.72      .9991
     472    473            0      .0000       27785.72      .9991
     473    474            0      .0000       27785.72      .9991
     474    475            0      .0000       27785.72      .9991
     475    476            0      .0000       27785.72      .9991
     476    477            0      .0000       27785.72      .9991
     477    478            0      .0000       27785.72      .9991
     478    479            0      .0000       27785.72      .9991
     479    480            0      .0000       27785.72      .9991
     480    481            0      .0000       27785.72      .9991
     481    482            0      .0000       27785.72      .9991
     482    483            0      .0000       27785.72      .9991
     483    484            0      .0000       27785.72      .9991
     484    485            0      .0000       27785.72      .9991
     485    486            0      .0000       27785.72      .9991
     486    487            0      .0000       27785.72      .9991
     487    488            0      .0000       27785.72      .9991
     488    489            0      .0000       27785.72      .9991
     489    490            0      .0000       27785.72      .9991
     490    491            0      .0000       27785.72      .9991
     491    492            0      .0000       27785.72      .9991
     492    493            0      .0000       27785.72      .9991
     493    494            0      .0000       27785.72      .9991
     494    495            0      .0000       27785.72      .9991
     495    496            0      .0000       27785.72      .9991
     496    497            0      .0000       27785.72      .9991
     497    498            0      .0000       27785.72      .9991
     498    499            0      .0000       27785.72      .9991
     499    500            0      .0000       27785.72      .9991


 MATRIX CALCULATIONS
 *******************


 Expression:

  mf02   = 0

 Result matrix:         mf02  : temp2                                         Temporary Matrix 2                                                               (18-11-21 10:40:05)

 Constraint matrix:     none

 Submatrix:             all origins             all destinations

 Number of expression evaluations:          35721
 Minimum expression value:                .000000   (at      1      1)
 Maximum expression value:                .000000   (at      1      1)
 Average expression value:                .000000
 Sum of expression values:                .000000


 MATRIX CALCULATIONS
 *******************


 Expression:

  mf02   = mf752' + mf753

 Result matrix:         mf02  : temp2                                         Temporary Matrix 2                                                               (18-11-21 10:40:05)

 Data matrices:         mf752 : PT251                                         CBLTM PT IP HBWIB Rail                                                           (18-11-21 10:39:35)
                        mf753 : PT252                                         CBLTM PT IP HBWOB Rail                                                           (18-11-21 10:39:35)

 Constraint matrix:     none

 Submatrix:             all origins             all destinations

 Number of expression evaluations:          35721
 Minimum expression value:                .000000   (at      1     22)
 Maximum expression value:           62228.460937   (at    106    106)
 Average expression value:               6.087094
 Sum of expression values:          217437.078125
