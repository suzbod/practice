

 Matrix histogram
 ----------------


 data   matrix        mf795 : DSTSK2                                   Distance Skim Car                                                               

 weight matrix        mf01  : temp1                                    Temporary Matrix 1                                                              

  all  origins   all  destinations

 constraint matrix    mf10  : temp10                                   Temporary Matrix 10                                                             
          interval  lower:         1
                    upper:         1  include 

 out of range       above:  158.336
                    below:        0

 statistics         mean : 23.49496
                    std  : 37.93603

    interval        ------density-------      -----cumulative-----
 (from  ,    to)    absolute  normalized      absolute  normalized

       0      1            0      .0000              0      .0000
       1      2      .727864      .0000        .727864      .0000
       2      3     88.02873      .0006        88.7566      .0006
       3      4     124.1409      .0008       212.8975      .0014
       4      5     4034.139      .0272       4247.037      .0286
       5      6     10561.78      .0712       14808.82      .0999
       6      7     12474.16      .0841       27282.98      .1840
       7      8     6286.244      .0424       33569.22      .2263
       8      9     6529.949      .0440       40099.17      .2704
       9     10     5037.239      .0340       45136.41      .3043
      10     11     5222.621      .0352       50359.03      .3396
      11     12     7456.529      .0503       57815.55      .3898
      12     13     5384.736      .0363       63200.29      .4261
      13     14     6834.855      .0461       70035.14      .4722
      14     15     4413.404      .0298       74448.55      .5020
      15     16     8282.116      .0558       82730.67      .5578
      16     17     8070.461      .0544       90801.13      .6122
      17     18     6369.976      .0430        97171.1      .6552
      18     19     5479.906      .0369         102651      .6921
      19     20     2543.957      .0172         105195      .7093
      20     21     1632.837      .0110       106827.8      .7203
      21     22     1818.056      .0123       108645.8      .7326
      22     23     2914.185      .0196         111560      .7522
      23     24     1940.367      .0131       113500.4      .7653
      24     25     2379.716      .0160       115880.1      .7813
      25     26      965.052      .0065       116845.2      .7879
      26     27     1929.991      .0130       118775.2      .8009
      27     28     1534.517      .0103       120309.7      .8112
      28     29     1044.187      .0070       121353.9      .8183
      29     30     2760.435      .0186       124114.3      .8369
      30     31     1279.456      .0086       125393.8      .8455
      31     32     1514.733      .0102       126908.5      .8557
      32     33     630.3933      .0043       127538.9      .8600
      33     34     706.1361      .0048         128245      .8647
      34     35     967.1747      .0065       129212.2      .8712
      35     36     1171.471      .0079       130383.7      .8791
      36     37     590.9948      .0040       130974.7      .8831
      37     38     1018.044      .0069       131992.7      .8900
      38     39     582.5568      .0039       132575.3      .8939
      39     40     330.7188      .0022         132906      .8961
      40     41     268.5064      .0018       133174.5      .8980
      41     42     494.5266      .0033         133669      .9013
      42     43     494.3959      .0033       134163.4      .9046
      43     44     496.1376      .0033       134659.5      .9080
      44     45     627.9652      .0042       135287.5      .9122
      45     46     291.5117      .0020         135579      .9142
      46     47      171.954      .0012         135751      .9153
      47     48      429.217      .0029       136180.2      .9182
      48     49     307.9798      .0021       136488.2      .9203
      49     50     443.5108      .0030       136931.7      .9233
      50     51      257.334      .0017         137189      .9250
      51     52     354.4786      .0024       137543.5      .9274
      52     53     222.7454      .0015       137766.3      .9289
      53     54     147.5663      .0010       137913.8      .9299
      54     55     296.7507      .0020       138210.6      .9319
      55     56     390.1327      .0026       138600.7      .9345
      56     57     307.6687      .0021       138908.4      .9366
      57     58     183.0577      .0012       139091.4      .9379
      58     59     232.1746      .0016       139323.6      .9394
      59     60     194.8341      .0013       139518.4      .9407
      60     61     152.3813      .0010       139670.8      .9418
      61     62     151.8913      .0010       139822.7      .9428
      62     63     131.3505      .0009         139954      .9437
      63     64     156.9689      .0011         140111      .9447
      64     65     130.3744      .0009       140241.4      .9456
      65     66      133.862      .0009       140375.2      .9465
      66     67     113.8976      .0008       140489.1      .9473
      67     68     193.2519      .0013       140682.4      .9486
      68     69     262.1137      .0018       140944.5      .9503
      69     70     155.9457      .0011       141100.5      .9514
      70     71     71.54381      .0005         141172      .9519
      71     72     154.0773      .0010       141326.1      .9529
      72     73     105.6484      .0007       141431.7      .9536
      73     74      105.729      .0007       141537.5      .9543
      74     75     130.3088      .0009       141667.8      .9552
      75     76     71.83309      .0005       141739.6      .9557
      76     77     95.17999      .0006       141834.8      .9564
      77     78     52.90011      .0004       141887.7      .9567
      78     79     47.21286      .0003       141934.9      .9570
      79     80     112.2237      .0008       142047.1      .9578
      80     81     105.2445      .0007       142152.4      .9585
      81     82     194.7512      .0013       142347.1      .9598
      82     83      55.1499      .0004       142402.3      .9602
      83     84     65.27008      .0004       142467.5      .9606
      84     85     37.10964      .0003       142504.7      .9609
      85     86     26.78305      .0002       142531.4      .9610
      86     87     133.9786      .0009       142665.4      .9620
      87     88      60.9983      .0004       142726.4      .9624
      88     89     24.06393      .0002       142750.5      .9625
      89     90      53.7506      .0004       142804.2      .9629
      90     91     148.3621      .0010       142952.6      .9639
      91     92     165.4481      .0011         143118      .9650
      92     93     85.40746      .0006       143203.5      .9656
      93     94     106.4561      .0007       143309.9      .9663
      94     95     16.75622      .0001       143326.7      .9664
      95     96     96.34912      .0006         143423      .9671
      96     97     46.85001      .0003       143469.8      .9674
      97     98     156.5162      .0011       143626.4      .9684
      98     99     88.43148      .0006       143714.8      .9690
      99    100     35.36661      .0002       143750.2      .9693
     100    101     68.35337      .0005       143818.5      .9697
     101    102     44.03912      .0003       143862.6      .9700
     102    103     41.66293      .0003       143904.2      .9703
     103    104     41.48393      .0003       143945.7      .9706
     104    105      91.5968      .0006       144037.3      .9712
     105    106     68.17289      .0005       144105.5      .9717
     106    107     30.00861      .0002       144135.5      .9719
     107    108     43.24126      .0003       144178.7      .9722
     108    109     24.00071      .0002       144202.7      .9723
     109    110     23.53199      .0002       144226.2      .9725
     110    111     52.63822      .0004       144278.9      .9728
     111    112     58.28264      .0004       144337.2      .9732
     112    113     50.46178      .0003       144387.6      .9736
     113    114     117.0408      .0008       144504.7      .9744
     114    115     24.68745      .0002       144529.4      .9745
     115    116     74.71224      .0005       144604.1      .9750
     116    117     41.56879      .0003       144645.7      .9753
     117    118     49.70172      .0003       144695.4      .9756
     118    119     91.50183      .0006       144786.9      .9763
     119    120      50.9469      .0003       144837.8      .9766
     120    121     49.05741      .0003       144886.9      .9769
     121    122     54.47507      .0004       144941.3      .9773
     122    123     56.18772      .0004       144997.5      .9777
     123    124     57.34481      .0004       145054.9      .9781
     124    125     57.15996      .0004         145112      .9784
     125    126     44.01632      .0003         145156      .9787
     126    127     42.41244      .0003       145198.5      .9790
     127    128     34.81318      .0002       145233.3      .9793
     128    129     42.92255      .0003       145276.2      .9796
     129    130     51.60294      .0003       145327.8      .9799
     130    131     33.91832      .0002       145361.7      .9801
     131    132     46.57558      .0003       145408.3      .9804
     132    133     41.15843      .0003       145449.5      .9807
     133    134     88.35807      .0006       145537.8      .9813
     134    135     39.57198      .0003       145577.4      .9816
     135    136     41.87128      .0003       145619.3      .9819
     136    137     39.28096      .0003       145658.5      .9821
     137    138     38.21894      .0003       145696.8      .9824
     138    139     51.99536      .0004       145748.8      .9827
     139    140     27.85547      .0002       145776.6      .9829
     140    141     43.77026      .0003       145820.4      .9832
     141    142     18.54167      .0001       145838.9      .9834
     142    143     51.01098      .0003         145890      .9837
     143    144     60.80656      .0004       145950.8      .9841
     144    145     36.68176      .0002       145987.5      .9844
     145    146     34.36991      .0002       146021.8      .9846
     146    147     48.38597      .0003       146070.2      .9849
     147    148     29.68186      .0002       146099.9      .9851
     148    149     22.89453      .0002       146122.8      .9853
     149    150     38.53267      .0003       146161.3      .9855
     150    151     26.95173      .0002       146188.3      .9857
     151    152     18.03239      .0001       146206.3      .9858
     152    153      25.5687      .0002       146231.9      .9860
     153    154     24.27345      .0002       146256.2      .9862
     154    155     16.88356      .0001         146273      .9863
     155    156     40.55952      .0003       146313.6      .9866
     156    157     51.00365      .0003       146364.6      .9869
     157    158      40.1437      .0003       146404.7      .9872
     158    159     36.53789      .0002       146441.3      .9874
     159    160     24.93462      .0002       146466.2      .9876
     160    161     15.11733      .0001       146481.3      .9877
     161    162     13.31758      .0001       146494.7      .9878
     162    163     35.63446      .0002       146530.3      .9880
     163    164     26.87637      .0002       146557.2      .9882
     164    165     26.83605      .0002         146584      .9884
     165    166     9.184349      .0001       146593.2      .9884
     166    167     5.515738      .0000       146598.7      .9885
     167    168     27.56502      .0002       146626.3      .9887
     168    169     21.59483      .0001       146647.9      .9888
     169    170     8.864988      .0001       146656.7      .9889
     170    171     27.27319      .0002         146684      .9890
     171    172            0      .0000         146684      .9890
     172    173     25.74687      .0002       146709.7      .9892
     173    174     22.15548      .0001       146731.9      .9894
     174    175     7.916012      .0001       146739.8      .9894
     175    176     3.554201      .0000       146743.4      .9894
     176    177     11.62617      .0001         146755      .9895
     177    178      2.47139      .0000       146757.5      .9895
     178    179     11.26588      .0001       146768.7      .9896
     179    180     3.166898      .0000       146771.9      .9896
     180    181     14.00997      .0001       146785.9      .9897
     181    182     12.23961      .0001       146798.2      .9898
     182    183     20.23411      .0001       146818.4      .9900
     183    184     9.727786      .0001       146828.1      .9900
     184    185      17.9901      .0001       146846.1      .9901
     185    186     1.388853      .0000       146847.5      .9902
     186    187     9.930832      .0001       146857.4      .9902
     187    188     13.18859      .0001       146870.6      .9903
     188    189     3.856984      .0000       146874.5      .9903
     189    190       .65137      .0000       146875.1      .9903
     190    191     4.177518      .0000       146879.3      .9904
     191    192     9.688806      .0001         146889      .9904
     192    193     15.27277      .0001       146904.3      .9905
     193    194     8.928648      .0001       146913.2      .9906
     194    195     2.232455      .0000       146915.4      .9906
     195    196     4.590015      .0000         146920      .9906
     196    197     4.143501      .0000       146924.2      .9907
     197    198      6.63081      .0000       146930.8      .9907
     198    199     13.72695      .0001       146944.5      .9908
     199    200     7.930443      .0001       146952.5      .9909
     200    201     4.999804      .0000       146957.5      .9909
     201    202     4.513007      .0000         146962      .9909
     202    203     2.341114      .0000       146964.3      .9909
     203    204     3.299087      .0000       146967.6      .9910
     204    205     5.706179      .0000       146973.3      .9910
     205    206     19.01189      .0001       146992.3      .9911
     206    207     5.055989      .0000       146997.4      .9912
     207    208     3.897321      .0000       147001.3      .9912
     208    209       16.428      .0001       147017.7      .9913
     209    210     3.480264      .0000       147021.2      .9913
     210    211     7.011076      .0000       147028.2      .9914
     211    212     7.627538      .0001       147035.8      .9914
     212    213     10.31189      .0001       147046.1      .9915
     213    214     8.175427      .0001       147054.3      .9915
     214    215     7.947114      .0001       147062.3      .9916
     215    216            0      .0000       147062.3      .9916
     216    217      .908792      .0000       147063.2      .9916
     217    218     4.509774      .0000       147067.7      .9916
     218    219      12.5358      .0001       147080.2      .9917
     219    220     4.851722      .0000       147085.1      .9918
     220    221     1.035196      .0000       147086.1      .9918
     221    222            0      .0000       147086.1      .9918
     222    223     2.747883      .0000       147088.9      .9918
     223    224     7.699885      .0001       147096.6      .9918
     224    225     4.835038      .0000       147101.4      .9919
     225    226            0      .0000       147101.4      .9919
     226    227     6.237968      .0000       147107.6      .9919
     227    228     2.793864      .0000       147110.4      .9919
     228    229      .800222      .0000       147111.2      .9919
     229    230     8.183091      .0001       147119.4      .9920
     230    231        .7957      .0000       147120.2      .9920
     231    232     5.268001      .0000       147125.5      .9920
     232    233      3.12022      .0000       147128.6      .9920
     233    234      10.0165      .0001       147138.6      .9921
     234    235     5.686923      .0000       147144.3      .9922
     235    236      5.78683      .0000       147150.1      .9922
     236    237     13.01288      .0001       147163.1      .9923
     237    238     3.254792      .0000       147166.3      .9923
     238    239     3.728572      .0000       147170.1      .9923
     239    240     6.743702      .0000       147176.8      .9924
     240    241            0      .0000       147176.8      .9924
     241    242     4.886982      .0000       147181.7      .9924
     242    243     14.76132      .0001       147196.5      .9925
     243    244     4.801693      .0000       147201.3      .9925
     244    245      .700339      .0000         147202      .9925
     245    246      6.03099      .0000         147208      .9926
     246    247     6.716891      .0000       147214.7      .9926
     247    248     7.442115      .0001       147222.2      .9927
     248    249     13.74619      .0001       147235.9      .9928
     249    250      3.43879      .0000       147239.4      .9928
     250    251     5.878206      .0000       147245.2      .9928
     251    252     7.186415      .0000       147252.4      .9929
     252    253     12.58408      .0001         147265      .9930
     253    254     20.22916      .0001       147285.2      .9931
     254    255     8.584212      .0001       147293.8      .9932
     255    256     4.145135      .0000         147298      .9932
     256    257     6.014098      .0000         147304      .9932
     257    258      3.48965      .0000       147307.5      .9933
     258    259     12.59982      .0001         147320      .9933
     259    260     18.87416      .0001       147338.9      .9935
     260    261      12.3461      .0001       147351.3      .9935
     261    262     9.831236      .0001       147361.1      .9936
     262    263     22.88982      .0002         147384      .9938
     263    264     13.55726      .0001       147397.5      .9939
     264    265     6.956181      .0000       147404.5      .9939
     265    266     10.72369      .0001       147415.2      .9940
     266    267     5.406736      .0000       147420.6      .9940
     267    268     12.16421      .0001       147432.8      .9941
     268    269     11.23161      .0001         147444      .9942
     269    270     8.902364      .0001       147452.9      .9942
     270    271     4.258761      .0000       147457.2      .9943
     271    272            0      .0000       147457.2      .9943
     272    273      .955684      .0000       147458.2      .9943
     273    274       19.173      .0001       147477.3      .9944
     274    275     16.66236      .0001         147494      .9945
     275    276     12.88486      .0001       147506.9      .9946
     276    277     11.18702      .0001       147518.1      .9947
     277    278     37.39543      .0003       147555.5      .9949
     278    279     2.911422      .0000       147558.4      .9949
     279    280     3.005619      .0000       147561.4      .9950
     280    281     16.30306      .0001       147577.7      .9951
     281    282     4.200424      .0000       147581.9      .9951
     282    283      8.19983      .0001       147590.1      .9952
     283    284     4.530275      .0000       147594.6      .9952
     284    285     1.577124      .0000       147596.2      .9952
     285    286      43.0807      .0003       147639.2      .9955
     286    287     4.833551      .0000       147644.1      .9955
     287    288     12.35018      .0001       147656.4      .9956
     288    289       .69848      .0000       147657.1      .9956
     289    290     2.836501      .0000         147660      .9956
     290    291     14.66365      .0001       147674.6      .9957
     291    292     2.488728      .0000       147677.1      .9957
     292    293      .698603      .0000       147677.8      .9957
     293    294            0      .0000       147677.8      .9957
     294    295     6.301866      .0000       147684.1      .9958
     295    296     18.74185      .0001       147702.8      .9959
     296    297     1.386222      .0000       147704.2      .9959
     297    298     17.42084      .0001       147721.7      .9960
     298    299     9.938941      .0001       147731.6      .9961
     299    300     5.570935      .0000       147737.2      .9961
     300    301      .810529      .0000         147738      .9962
     301    302     1.601042      .0000       147739.6      .9962
     302    303            0      .0000       147739.6      .9962
     303    304     7.683279      .0001       147747.3      .9962
     304    305     3.336304      .0000       147750.6      .9962
     305    306     18.05731      .0001       147768.7      .9964
     306    307     4.715397      .0000       147773.4      .9964
     307    308     13.31101      .0001       147786.7      .9965
     308    309     7.051958      .0000       147793.7      .9965
     309    310     4.373569      .0000       147798.1      .9966
     310    311     2.251301      .0000       147800.4      .9966
     311    312     10.95793      .0001       147811.3      .9966
     312    313     1.860482      .0000       147813.2      .9967
     313    314     6.543241      .0000       147819.7      .9967
     314    315     5.466486      .0000       147825.2      .9967
     315    316     19.80377      .0001         147845      .9969
     316    317            0      .0000         147845      .9969
     317    318     5.433501      .0000       147850.4      .9969
     318    319     9.151484      .0001       147859.6      .9970
     319    320     12.50922      .0001       147872.1      .9971
     320    321     14.11697      .0001       147886.2      .9972
     321    322     5.776466      .0000         147892      .9972
     322    323     6.932644      .0000       147898.9      .9972
     323    324      .850589      .0000       147899.8      .9972
     324    325     11.30821      .0001       147911.1      .9973
     325    326     13.87685      .0001         147925      .9974
     326    327      8.46016      .0001       147933.4      .9975
     327    328     4.361136      .0000       147937.8      .9975
     328    329            0      .0000       147937.8      .9975
     329    330     1.863954      .0000       147939.6      .9975
     330    331     15.77998      .0001       147955.4      .9976
     331    332     19.50364      .0001       147974.9      .9978
     332    333            0      .0000       147974.9      .9978
     333    334     25.38997      .0002       148000.3      .9979
     334    335            0      .0000       148000.3      .9979
     335    336            0      .0000       148000.3      .9979
     336    337            0      .0000       148000.3      .9979
     337    338            0      .0000       148000.3      .9979
     338    339     3.420316      .0000       148003.7      .9979
     339    340            0      .0000       148003.7      .9979
     340    341            0      .0000       148003.7      .9979
     341    342            0      .0000       148003.7      .9979
     342    343            0      .0000       148003.7      .9979
     343    344     40.61038      .0003       148044.3      .9982
     344    345            0      .0000       148044.3      .9982
     345    346            0      .0000       148044.3      .9982
     346    347            0      .0000       148044.3      .9982
     347    348            0      .0000       148044.3      .9982
     348    349            0      .0000       148044.3      .9982
     349    350            0      .0000       148044.3      .9982
     350    351            0      .0000       148044.3      .9982
     351    352            0      .0000       148044.3      .9982
     352    353            0      .0000       148044.3      .9982
     353    354            0      .0000       148044.3      .9982
     354    355      .678469      .0000         148045      .9982
     355    356            0      .0000         148045      .9982
     356    357            0      .0000         148045      .9982
     357    358     12.41807      .0001       148057.4      .9983
     358    359            0      .0000       148057.4      .9983
     359    360            0      .0000       148057.4      .9983
     360    361            0      .0000       148057.4      .9983
     361    362            0      .0000       148057.4      .9983
     362    363            0      .0000       148057.4      .9983
     363    364            0      .0000       148057.4      .9983
     364    365            0      .0000       148057.4      .9983
     365    366            0      .0000       148057.4      .9983
     366    367            0      .0000       148057.4      .9983
     367    368     2.606846      .0000         148060      .9983
     368    369     1.670593      .0000       148061.7      .9983
     369    370     1.172147      .0000       148062.9      .9983
     370    371            0      .0000       148062.9      .9983
     371    372            0      .0000       148062.9      .9983
     372    373     4.224179      .0000       148067.1      .9984
     373    374            0      .0000       148067.1      .9984
     374    375            0      .0000       148067.1      .9984
     375    376            0      .0000       148067.1      .9984
     376    377            0      .0000       148067.1      .9984
     377    378            0      .0000       148067.1      .9984
     378    379     6.506786      .0000       148073.6      .9984
     379    380            0      .0000       148073.6      .9984
     380    381      .879047      .0000       148074.5      .9984
     381    382            0      .0000       148074.5      .9984
     382    383            0      .0000       148074.5      .9984
     383    384     4.446263      .0000       148078.9      .9985
     384    385            0      .0000       148078.9      .9985
     385    386            0      .0000       148078.9      .9985
     386    387     2.079211      .0000         148081      .9985
     387    388            0      .0000         148081      .9985
     388    389     2.909955      .0000       148083.9      .9985
     389    390       1.4008      .0000       148085.3      .9985
     390    391     4.337309      .0000       148089.7      .9985
     391    392            0      .0000       148089.7      .9985
     392    393            0      .0000       148089.7      .9985
     393    394      3.99496      .0000       148093.7      .9986
     394    395     1.607089      .0000       148095.3      .9986
     395    396            0      .0000       148095.3      .9986
     396    397     3.729051      .0000         148099      .9986
     397    398     8.533607      .0001       148107.5      .9986
     398    399      1.51039      .0000       148109.1      .9987
     399    400            0      .0000       148109.1      .9987
     400    401            0      .0000       148109.1      .9987
     401    402            0      .0000       148109.1      .9987
     402    403     4.714447      .0000       148113.8      .9987
     403    404     2.814933      .0000       148116.6      .9987
     404    405      .809603      .0000       148117.4      .9987
     405    406            0      .0000       148117.4      .9987
     406    407     1.609503      .0000         148119      .9987
     407    408     3.931416      .0000         148123      .9988
     408    409            0      .0000         148123      .9988
     409    410      2.51276      .0000       148125.5      .9988
     410    411     8.324757      .0001       148133.8      .9988
     411    412            0      .0000       148133.8      .9988
     412    413      .804899      .0000       148134.6      .9988
     413    414            0      .0000       148134.6      .9988
     414    415     15.23882      .0001       148149.8      .9989
     415    416            0      .0000       148149.8      .9989
     416    417            0      .0000       148149.8      .9989
     417    418            0      .0000       148149.8      .9989
     418    419            0      .0000       148149.8      .9989
     419    420            0      .0000       148149.8      .9989
     420    421            0      .0000       148149.8      .9989
     421    422            0      .0000       148149.8      .9989
     422    423            0      .0000       148149.8      .9989
     423    424            0      .0000       148149.8      .9989
     424    425            0      .0000       148149.8      .9989
     425    426            0      .0000       148149.8      .9989
     426    427            0      .0000       148149.8      .9989
     427    428            0      .0000       148149.8      .9989
     428    429            0      .0000       148149.8      .9989
     429    430            0      .0000       148149.8      .9989
     430    431            0      .0000       148149.8      .9989
     431    432            0      .0000       148149.8      .9989
     432    433            0      .0000       148149.8      .9989
     433    434            0      .0000       148149.8      .9989
     434    435            0      .0000       148149.8      .9989
     435    436            0      .0000       148149.8      .9989
     436    437            0      .0000       148149.8      .9989
     437    438            0      .0000       148149.8      .9989
     438    439            0      .0000       148149.8      .9989
     439    440            0      .0000       148149.8      .9989
     440    441            0      .0000       148149.8      .9989
     441    442            0      .0000       148149.8      .9989
     442    443            0      .0000       148149.8      .9989
     443    444            0      .0000       148149.8      .9989
     444    445            0      .0000       148149.8      .9989
     445    446            0      .0000       148149.8      .9989
     446    447            0      .0000       148149.8      .9989
     447    448            0      .0000       148149.8      .9989
     448    449            0      .0000       148149.8      .9989
     449    450            0      .0000       148149.8      .9989
     450    451            0      .0000       148149.8      .9989
     451    452            0      .0000       148149.8      .9989
     452    453            0      .0000       148149.8      .9989
     453    454            0      .0000       148149.8      .9989
     454    455            0      .0000       148149.8      .9989
     455    456            0      .0000       148149.8      .9989
     456    457            0      .0000       148149.8      .9989
     457    458            0      .0000       148149.8      .9989
     458    459            0      .0000       148149.8      .9989
     459    460            0      .0000       148149.8      .9989
     460    461            0      .0000       148149.8      .9989
     461    462            0      .0000       148149.8      .9989
     462    463            0      .0000       148149.8      .9989
     463    464            0      .0000       148149.8      .9989
     464    465            0      .0000       148149.8      .9989
     465    466            0      .0000       148149.8      .9989
     466    467            0      .0000       148149.8      .9989
     467    468            0      .0000       148149.8      .9989
     468    469            0      .0000       148149.8      .9989
     469    470            0      .0000       148149.8      .9989
     470    471            0      .0000       148149.8      .9989
     471    472            0      .0000       148149.8      .9989
     472    473            0      .0000       148149.8      .9989
     473    474            0      .0000       148149.8      .9989
     474    475            0      .0000       148149.8      .9989
     475    476            0      .0000       148149.8      .9989
     476    477            0      .0000       148149.8      .9989
     477    478            0      .0000       148149.8      .9989
     478    479            0      .0000       148149.8      .9989
     479    480            0      .0000       148149.8      .9989
     480    481            0      .0000       148149.8      .9989
     481    482            0      .0000       148149.8      .9989
     482    483            0      .0000       148149.8      .9989
     483    484            0      .0000       148149.8      .9989
     484    485            0      .0000       148149.8      .9989
     485    486            0      .0000       148149.8      .9989
     486    487            0      .0000       148149.8      .9989
     487    488            0      .0000       148149.8      .9989
     488    489            0      .0000       148149.8      .9989
     489    490            0      .0000       148149.8      .9989
     490    491            0      .0000       148149.8      .9989
     491    492            0      .0000       148149.8      .9989
     492    493            0      .0000       148149.8      .9989
     493    494            0      .0000       148149.8      .9989
     494    495            0      .0000       148149.8      .9989
     495    496            0      .0000       148149.8      .9989
     496    497            0      .0000       148149.8      .9989
     497    498            0      .0000       148149.8      .9989
     498    499            0      .0000       148149.8      .9989
     499    500            0      .0000       148149.8      .9989
